<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Hash;
use App\Http\Transformers\ResponseTransformer; 
use DB;
use App\Http\Models\VoucherModel;
use Carbon\Carbon;
use Illuminate\Support\MessageBag;
use App\Http\Models\CustomerModel;

class VoucherController extends Controller
{

    private function _getCustomer(Request $request){
        $customer = CustomerModel::firstOrCreate([
            "email" => $request->email
        ],[
            "first_name"        =>  $request->first_name,
            "last_name"         =>  $request->last_name,
            "gender"            =>  $request->gender,
            "contact_number"    =>  $request->contact_number,
            "email"             =>  $request->email,
            "date_of_bird"      =>  $request->date_of_bird
        ]);

        return $customer;
    }

    public function claimVoucher(Request $request)
    {
        $transaction_period = 30;       // in day
        $min_transaction    = 3;        // 
        $min_total_spent    = 100;      // in USD 
        $keep_voucher_for   = 5;        // in minutes
        $max_voucher_per_user = 1;      // voucher per user
        
        // GET AVAILABLE VOUCHER
        $available_voucher = new VoucherModel;
        $available_voucher = $available_voucher->whereNull('purechase_id');
        $available_voucher = $available_voucher->whereNull('customer_id');
        $available_voucher = $available_voucher->whereNull('valid_until')
                             ->orWhere('valid_until','<',date('Y-m-d H:i:s'));

        $available_voucher = $available_voucher->first();

        // IF VOUCHER NOT AVAILABLE
        if($available_voucher == null)
            return (new ResponseTransformer)->toJson(400,'Vouchers are not available',false);
            
        $available_voucher->valid_until = carbon::now()->add(500,'milliseconds');
        $available_voucher->update();
    
        try {

        DB::beginTransaction();

            $user_detail = $this->_getCustomer($request);

            // Check (Each customer is allowed to redeem $max_voucher_per_user cash voucher only.)
            if($user_detail->Voucher && $user_detail->Voucher->count() >= $max_voucher_per_user)
                return (new ResponseTransformer)->toJson(400,'Promo valid for 1 vouchers per user',false);

            $user_transaction_count = 0;
            $user_transaction_spend = 0;

            if($user_detail->Transaction){
                $start_date = Carbon::now()->subDays($transaction_period)->format('Y-m-d H:i:s');
                $end_date   = Carbon::now()->format('Y-m-d H:i:s');

                $user_transaction = $user_detail->Transaction
                                    ->where('transaction_at','<=',$end_date)
                                    ->where('transaction_at','>=',$start_date);
                $user_transaction_count = $user_transaction->count();
                $user_transaction_spend = $user_transaction->sum('total_spend');
            }

            // dd($user_transaction_count,$user_transaction_spend,$end_date,$start_date);
            
            //  check (Complete $min_transaction purchase transactions within the last $transaction_period days.)
            if($user_transaction_count  < $min_transaction)
                return (new ResponseTransformer)->toJson(400,'Increase your transaction to get a Voucher.',["error_code" => "ERR0001"]);

            //  check (Total transactions equal or more than $min_total_spent.)
            if($user_transaction_spend  < $min_total_spent)
                return (new ResponseTransformer)->toJson(400,'Increase your transaction to get a Voucher.',["error_code" => "ERR0002"]);

            $available_voucher->customer_id = $user_detail->id;
            $available_voucher->valid_until = carbon::now()->add($keep_voucher_for,'minutes');

            if(!$available_voucher->update())
                return (new ResponseTransformer)->toJson(400,'failed',false);
  
        DB::commit();

            $data    = new \stdClass();
            $data->voucher_code = $available_voucher->voucher_code;

            return (new ResponseTransformer)->toJson(200,'success',$data);

        } catch (\exception $exception) {

            DB::rollBack();
            VoucherModel::where('id',$available_voucher->id)->update(['valid_until' => null]);
 
            return (new ResponseTransformer)->toJson(500,'Server Error',$exception->getMessage());
        }
    }

}