<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherModel extends Model
{
    use HasFactory;
    protected $table    = "vouchers";
    protected $fillable = [
        "id", "customer_id", "purechase_id", "voucher_code", "valid_until", "created_at", "updated_at"
    ]; 
}
