<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    use HasFactory;
    protected $table = "customers";
    protected $fillable = [
        "email",
        "first_name",
        "last_name",
        "gender",
        "contact_number",
        "email",
        "date_of_bird"
    ];

    public function Voucher(){
        return $this->hasMany('App\Http\Models\VoucherModel','customer_id','id');
    }

    public function Transaction(){
        return $this->hasMany('App\Http\Models\PurechaseTransactionModel','customer_id','id');
    }
}
