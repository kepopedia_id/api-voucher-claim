<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurechaseTransactionModel extends Model
{
    use HasFactory;
    protected $table = "purechase_transaction"; 
}
