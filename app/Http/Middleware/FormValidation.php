<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use App\Http\Transformers\ResponseTransformer;
use Illuminate\Support\Facades\Route;

class FormValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { 

        $route_name = Route::currentRouteName();
        $validation = $this->_selector($route_name);

        $validator = app()->make('validator');
        $validate = $validator->make($request->all(),$validation);

        if($validate->fails()){
          return (new ResponseTransformer)->toJson(400,'Error Input', $validate->errors());
        }else {
          return $next($request);
        }
    }

    private function _selector($route){
      switch ($route) { 

        // VOUCHER
        case 'PostVoucherControllerClaimVoucher':
            return [
              'first_name' => 'required',
              'last_name'  => 'required',
              'email' => 'required|email|max:50',
              'gender' => 'required|in:male,female',
              'contact_number' => 'required|max:50',
              'date_of_bird'   => 'required|date_format:Y-m-d|before:'.date('Y-m-d'),
              'photo'          => 'required'
            ];
        break; 

        default:
          return [];
      }
    }
}
