<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurechaseTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purechase_transaction', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("customer_id");
            $table->double('total_spend',10,2);
            $table->double('total_saving',10,2);
            $table->timestamp('transaction_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purechase_transaction');
    }
}
