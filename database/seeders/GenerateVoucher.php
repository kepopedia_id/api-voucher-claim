<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Models\VoucherModel;


class GenerateVoucher extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limit = env('VOUCHER_GENERATE_LIMIT', 1); 
        $prefix = env('VOUCHER_GENERATE_PREFIX',"VOUCHER"); 

        $delete = VoucherModel::truncate();

        $insert = [];

        for($i = 1 ;$i <= $limit ; $i++){
            $insert[] = [
                "voucher_code" =>  $prefix.$i
            ];
        }
            VoucherModel::insert($insert);
    }

}
