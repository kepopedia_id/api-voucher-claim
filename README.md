
# [Claim Voucher Simple Apps]
 
## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/8.x/installation)

Clone the repository

    git clone https://andhisaputro@bitbucket.org/andhisaputro/aichattest.git
    
Switch to the repo folder

    cd aichattest
    
Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations in all folder (**Set the database connection in .env before migrating**)

    php artisan migrate --path=database/migrations/*

Start the local development server

    php artisan serve 


## Testing

### Available Parameter for Unit Testing

 
| ENV NAME | DEFAULT | DESCRIPTION 
|--|--|--|
| VOUCHER_GENERATE_PREFIX | VOUCHER | prefix for your voucher |
| VOUCHER_GENERATE_LIMIT | 1 | Number of voucher do you want to genrate |
| UNIT_TEST_POSITIF | 1 | Number of user able to claim voucher |
| UNIT_TEST_ATTEMP | 1 | Number of attempt you want to run a test | 


 
  
### Example Run Unit Testing
#### Test bellow mean every user will get 1 voucher. Total 5000 User and 5000 Voucher

    VOUCHER_GENERATE_PREFIX=COUPON VOUCHER_GENERATE_LIMIT=5000 UNIT_TEST_POSITIF=5000 UNIT_TEST_ATTEMP=5000  phpunit tests/Feature/AppHttpControllersControllerVoucherController.php
    
Result : 
![enter image description here](https://lingkerid.nyc3.digitaloceanspaces.com/saputroandhi/images/others/Screenshot%202021-03-10%20at%2016.37.18.png)

#### Of the 500 users who have the right to get a voucher, only 1 will get a voucher because there is only 1 voucher in the database 

    VOUCHER_GENERATE_PREFIX=COUPON VOUCHER_GENERATE_LIMIT=1 UNIT_TEST_POSITIF=500 UNIT_TEST_ATTEMP=500  phpunit tests/Feature/AppHttpControllersControllerVoucherController.php

result:
![result](https://lingkerid.nyc3.digitaloceanspaces.com/saputroandhi/images/others/Screenshot%202021-03-10%20at%2016.37.52.png)