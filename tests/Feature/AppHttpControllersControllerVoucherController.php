<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Models\VoucherModel;
use App\Http\Models\CustomerModel;
use App\Http\Models\PurechaseTransactionModel;
use Carbon\Carbon;

class AppHttpControllersControllerVoucherController extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_claim()
    {
        $attemp         = env('UNIT_TEST_ATTEMP', 1);
        $voucher_total  = env('VOUCHER_GENERATE_LIMIT', 1);
        $positif        = env('UNIT_TEST_POSITIF', 0);

        $this->seed('GenerateVoucher');

        $datas = $this->_generateUser($attemp,$positif);
        foreach($datas as $data){
            $response = $this->post('/api/voucher/claim',[
                "photo" =>  "dumy",
                "first_name" =>  $data['first_name'],
                "last_name" => $data['last_name'],
                "email" =>  $data['email'],
                "gender" =>  $data['gender'],
                "contact_number" =>  $data['contact_number'],
                "date_of_bird" =>  Carbon::parse($data['date_of_bird'])->format('Y-m-d')
            ]);
 
            if($data['positif'] == true ){ 
                if($data['attemp'] <= $voucher_total)
                    $response->assertStatus(200);


                if($data['attemp'] > $voucher_total)
                    $response->assertStatus(400); 
            }
  
            if($data['positif'] == false){ 
                $response->assertStatus(400);
            }
        }

        VoucherModel::truncate();
        CustomerModel::truncate();
        PurechaseTransactionModel::truncate();
    }

    private function _generateUser($total_user,$total_user_can_claim){ 

        $datas = [];

        for($i = 1; $i <= $total_user ;$i++){
            $insert  = [ 
                "first_name"=> "first_name".$i,
                "last_name" => "last_name".$i,
                "email" =>  "email".$i."@gmail.com",
                "gender" => "male",
                "contact_number" => "67871256371".$i,
                "date_of_bird" => Carbon::now()->subDays(2)
            ]; 
            $user_id = CustomerModel::insertGetId($insert); 

            $insert['attemp'] = $i; 
            if($i <= $total_user_can_claim){
                $insert['positif'] = true;
                $this->_generateTransaction($user_id);
            }else{
                $insert['positif'] = false; 
            }
                  
            $insert['id']      =  $user_id;
            $datas[]= $insert;  
        }
        
        return $datas;
    }

    private function _generateVoucher(){
        
    }

    private function _generateTransaction($id){
        $data = [
           [
            "customer_id" => $id,
            "total_spend" => 105,
            "total_saving" => 0,
            "transaction_at" => Carbon::now()->format('Y-m-d H:i:s')
           ],
           [
            "customer_id" => $id,
            "total_spend" => 105,
            "total_saving" => 0,
            "transaction_at" => Carbon::now()->format('Y-m-d H:i:s')
           ],
           [
            "customer_id" => $id,
            "total_spend" => 105,
            "total_saving" => 0,
            "transaction_at" => Carbon::now()->format('Y-m-d H:i:s')
           ]
        ];
        $transaction = PurechaseTransactionModel::insert($data);
    }

}
